public class RemoteController {
    //название устройства
    private Tv tv;

    public RemoteController(Tv tv) {
        this.tv = tv;
    }

    public Channel getChannelByNumber(int ChannelNumber){
        return tv.getChannelByNumber(ChannelNumber);
    }
}
