public class Program {
    //название программы
    private String programName;

    //конструктор
    public Program(String programName) {
        this.programName = programName;
    }

    private Channel channel;

    //просим канал нас пустить
    public void goToChannel(Channel channel) {
        channel.addProgram(this);
    }

    public String getProgramName() {
        return programName;
    }
}
