public class Channel {

    //название канала
    private String channelName;

    //максимальное количество программ
    private static final int MAX_NUMBER_PROGRAM = 3;

    //массив пограмм
    public Program[] programs;

    //текущее количество программ
    private int programsCount = 0;

    public Channel(String channelName) {
        this.channelName = channelName;
        this.programs = new Program[MAX_NUMBER_PROGRAM];
    }

    //добавляем программу в канал
    public void addProgram(Program program) {
        if (programsCount < MAX_NUMBER_PROGRAM) {
            programs[programsCount] = program;
            programsCount++;
        }
    }

    //геттер
    public String getChannelName() {
        return channelName;
    }

    //сеттер
    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getRandomProgram() {
        int random = (int)(Math.random() * MAX_NUMBER_PROGRAM);
        return programs[random].getProgramName();
    }
}
