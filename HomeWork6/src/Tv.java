public class Tv {
    //название Тв
    private String nameTV;

    //максимальное количество каналов
    private static final int MAX_NUMBER_CHANNEL = 2;

    //массив каналв
    public Channel[] channels;

    //текущее количество каналов
    private int channelsCount = 0;

    //сделали место для 3 каналов
    public Tv(String nameTV) {
        this.nameTV = nameTV;
        this.channels = new Channel[MAX_NUMBER_CHANNEL];
    }

    //добавляем канал в тв
    public void addChannel(Channel channel) {
        if (channelsCount < MAX_NUMBER_CHANNEL) {
            channels[channelsCount] = channel;
            channelsCount++;
        }
    }

    //геттер
    public String getNameTV() {
        return nameTV;
    }

    public Channel getChannelByNumber(int channelNumber) {
        if (channelNumber < MAX_NUMBER_CHANNEL) {
            return channels[channelNumber];
        }
        return null;
    }
}
