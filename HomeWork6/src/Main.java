public class Main {
    public static void main(String[] args) {
        Tv tv = new Tv("Sony");

        //каналы
        Channel ch1 = new Channel("1 Канал");
        Channel ch2 = new Channel("Россия");
        Channel ch3 = new Channel("Матч ТВ");


        // программы
        Program p1 = new Program("Новости");
        Program p2 = new Program("Жить здорово");
        Program p3 = new Program("Модный приговор");
        Program p4 = new Program("Пусть говорят");
        Program p5 = new Program("Крепкий орешек К/ф");
        Program p6 = new Program("Давай поженимся");
        Program p7 = new Program("Доброе утро");
        Program p8 = new Program("Мужское/Женское");
        Program p9 = new Program("Вечерний Ургант");
        Program p10 = new Program("Маша и Медведь М/ф");
        Program p11 = new Program("Т/ш На ножах");
        Program p12 = new Program("Т/ш КВН");


        p1.goToChannel(ch1);
        p2.goToChannel(ch1);
        p3.goToChannel(ch1);
        p4.goToChannel(ch1);

        p5.goToChannel(ch2);
        p6.goToChannel(ch2);
        p7.goToChannel(ch2);
        p8.goToChannel(ch2);

        p9.goToChannel(ch3);
        p10.goToChannel(ch3);
        p11.goToChannel(ch3);
        p12.goToChannel(ch3);

        tv.addChannel(ch1);
        tv.addChannel(ch2);
        tv.addChannel(ch3);

        Channel channel = tv.getChannelByNumber(1);
        if (channel != null) {
            System.out.println(channel.getRandomProgram());
        }
    }
}
