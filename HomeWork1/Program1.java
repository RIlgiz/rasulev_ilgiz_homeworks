class Program1 {
    public static void main(String args[]) {
        int number = 12345;

        int a = number / 10; //1234
        int b = number - a * 10; //5

        int c = number / 100; //123
        int d = number - c * 100; //45
        int e = d / 10; //4

        int f = number / 1000; //12
        int j = number - f * 1000; //345
        int h = j / 100; //3

        int i = number / 1000; //12
        int k = i / 10; //1
        int l = i - k * 10; //2
        int m = number / 10000; //1

        int sum = m + l + h + e + b;

        System.out.println(sum);
    }
}
