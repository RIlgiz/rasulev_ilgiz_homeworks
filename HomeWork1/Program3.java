class Program3 {
    public static void main(String[] args) {
        int number1 = 119, number2 = 33542, number3 = 99, number4 = 9911, number5 = 0;
        int sum1 = 11, sum2 = 17, sum3 = 18, sum4 = 20, sum5 = 0;
        boolean isCondition1 = sum1 % 2 == 1, isCondition2 = sum2 % 2 == 1, isCondition3 = sum3 % 2 == 1, isCondition4 = sum4 % 2 == 1, isCondition5 = sum5 % 2 == 1;
        int x = 0;

        if (isCondition1) {
            x = x + number1;
        }

        if (isCondition2) {
            x = x * number2;
        }

        if (isCondition3) {
            x = x * number3;
        }

        if (isCondition4) {
            x = x * number4;
        }

        if (isCondition5) {
            x = x * number5;
        }

        System.out.print(x);
    }
}
