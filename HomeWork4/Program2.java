class Program2 {

    public static boolean binSearch(int[] ages, int numberForSearch, int left, int right) {
        //int middle = left + (right - left) / 2;
        int middle = (left + right) / 2;

        if (numberForSearch > ages[middle]) {
            left = middle + 1;
        } else {
            right = middle - 1;
        }

        if (ages[middle] == numberForSearch) {
            return true;
        } else {
            if (left <= right) {
                return binSearch(ages, numberForSearch, left, right);
            } else {
                return false;
            }
        }
    }

    public static void main(String[] args) {
        int a[] = {7, 18, 30, 44, 77, 81, 90};
        int b = 7;
        int c = 0;
        int d = a.length - 1;
        System.out.println(binSearch(a, b, c, d));
    }
}
