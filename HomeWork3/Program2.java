import java.util.Scanner;
import java.util.Arrays;

class Program2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int maxAgesCount = scanner.nextInt();

        int ages[] = new int[maxAgesCount];

        for (int i = 0; i < ages.length; i++) {
            ages[i] = scanner.nextInt();
        }

        int left = 0;
        int right = ages.length - 1;
        int middle = left + (right - left) / 2;
        int temp;
        int j = 0;

        for (int k = right; k > middle; k--) {
            temp = ages[k];
            ages[k] = ages[j];
            ages[j] = temp;
            j++;
        }

        System.out.println(Arrays.toString(ages));
    }
}
