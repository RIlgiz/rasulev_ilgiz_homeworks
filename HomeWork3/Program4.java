import java.util.Scanner;
import java.util.Arrays;

class Program4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int maxAgesCount = scanner.nextInt();

        int ages[] = new int[maxAgesCount];

        int i = 0;

        while (i < ages.length) {
            ages[i] = scanner.nextInt();
            i++;
        }

        System.out.println(Arrays.toString(ages));

        int temp;
        int positionOfMin;
        int min;

        for (int j = 0; j < ages.length; j++) {
            positionOfMin = j;
            min = ages[j];

            for (int k = j; k < ages.length; k++) {
                if (ages[k] < min) {
                    min = ages[k];
                    positionOfMin = k;
                }
            }

            temp = ages[j];
            ages[j] = ages[positionOfMin];
            ages[positionOfMin] = temp;

            System.out.println("MIN - " + min);
            System.out.println("POSITION OF MIN - " + positionOfMin);
            System.out.println(Arrays.toString(ages));
        }
    }
}
