import java.util.Scanner;
import java.util.Arrays;

class Program5 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int maxAgesCount = scanner.nextInt();

        int ages[] = new int[maxAgesCount];

        for (int i = 0; i < ages.length; i++) {
            ages[i] = scanner.nextInt();
        }

        for (int i = ages.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (ages[j] > ages[j + 1]) {
                    int temp = ages[j];
                    ages[j] = ages[j + 1];
                    ages[j + 1] = temp;
                }
            }
        }
        System.out.println(Arrays.toString(ages));
    }
}
