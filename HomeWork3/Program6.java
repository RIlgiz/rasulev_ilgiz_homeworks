import java.util.Scanner;

class Program6 {
    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);

        int maxAgesCount = scanner.nextInt();

        int ages[] = new int[maxAgesCount];

        for (int i = 0; i < ages.length; i++) {
            ages[i] = scanner.nextInt();
        }

        long number = 0;
        int offset = 10;


        for (int i = 0; i < ages.length; i++) {
            while (ages[i] >= offset) {
                offset = offset * 10;
            }

            number = number * offset + ages[i];
            offset = 10;
        }

        System.out.println(number);
    }
}
